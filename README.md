# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version (ruby 2.6.1)

* System dependencies (mysql 5.7.26)

* Configuration
    -
    
     Create local_env.yml and add the following variables with your own username, password, and database name:
      
            touch config/local_env.yml
     add:
        
      DATA_BASE_USERNAME: DATA_BASE_USER_NAME
      DATA_BASE_PASSWORD: DATA_BASE_PASSWORD
      DATA_BASE_NAME: DATA_BASE_NAME

* Database creation
    -
    To create your database:

        Rake db:create

    To run migrations:
    -
        rails db:migrate
        
* to Run The application:
    -
        - bundle install
        - rails s

* Packages
   -
     * active storage :
     
            -rails active_storage:install
            -rails db:migrate
       
     * active Storage Validations
     
     * Validates Timeliness:
     
            gem 'validates_timeliness', '~> 4.0', '>= 4.0.2'
            -rails generate validates_timeliness:install
     * JWT:
            
           gem 'jwt'
           - bundle install
     * bcrypt:
              
           gem 'bcrypt', '~> 3.1.7'
           - bundle install
     * Simple command:
                      
           gem 'simple_command'
           - bundle install
     
* How to run the test suite:
    -
    To run user model test:
    
        - rails test test/models/user_test.rb 
        
* How to use the api: 
    -
    * To Register needed json keys:
         
         - route: http://localhost:3000/users
              
                  {
                   first_name:xxxxx,
                   last_name:xxxxxx,
                   country_code: one of them {EGY FR GER},
                   phone_number: +xxxxxxxxxxxxxxx,
                   gender: one of them {male female},
                   birthdate: yyyy-mm-dd,
                   email: valid mail format,
                   avatar:one of these formats {png jpg jepg},
                   password:xxxxxxxxxx,
                   password_confirmation:xxxxxxxxxx
                  }
              
    * To Authenticate:
        
        - route: http://localhost:3000/authenticate
        
              {
                phone_number:+xxxxxxxxxxxxxxx,
                password:xxxxxxxxxx
              }
    * To update user status:
        
        - route: http://localhost:3000/update_status
    
              - put auth token in header
              
              {
                 phone_number:+xxxxxxxxxxxxxxx,
                 status:xxxxxx
              }