# frozen_string_literal: true

require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  test 'should  create user' do
    @user = User.new(first_name: 'ahmed',
                     last_name: 'ibrahim',
                     country_code: 'EGY',
                     phone_number: '+201111831133',
                     gender: 'male',
                     birthdate: '1994-02-13',
                     email: 'ah_ibrahim94@hotmail.com',
                     password:"12345",
                     password_confirmation:"12345")
    @user.avatar.attach(io: File.open('public/test_image.png'),
                        filename: 'test_image.png', content_type: 'image/png')
    assert @user.save
  end

  test 'should not create user without first_name' do
    @user = User.new(last_name: 'ibrahim',
                     country_code: 'EGY',
                     phone_number: '+201111831133',
                     gender: 'male',
                     birthdate: '1994-02-13',
                     email: 'ah_ibrahim94@hotmail.com')
    @user.avatar.attach(io: File.open('public/test_image.png'),
                        filename: 'test_image.png', content_type: 'image/png')
    assert_not @user.save
  end

  test 'should not create user without last_name' do
    @user = User.new(first_name: 'ahmed',
                     country_code: 'EGY',
                     phone_number: '+201111831133',
                     gender: 'male',
                     birthdate: '1994-02-13',
                     email: 'ah_ibrahim94@hotmail.com')
    @user.avatar.attach(io: File.open("public/test_image.png"),
                        filename: "test_image.png", content_type: "image/png")
    assert_not @user.save
  end
  test 'should not create user without email' do
    @user = User.new(first_name: 'ahmed',
                     last_name: 'ibrahim',
                     country_code: 'EGY',
                     phone_number: '+201111831133',
                     gender: 'male',
                     birthdate: '1994-02-13')
    @user.avatar.attach(io: File.open("public/test_image.png"),
                        filename: "test_image.png", content_type: "image/png")
    assert_not @user.save
  end

  test 'should not create user with invalid email' do
    @user = User.new(first_name: 'ahmed',
                     last_name: 'ibrahim',
                     country_code: 'EGY',
                     phone_number: '+201111831133',
                     gender: 'male',
                     birthdate: '1994-02-13',
                     email: 'sadkljfsf')
    @user.avatar.attach(io: File.open("public/test_image.png"),
                        filename: "test_image.png", content_type: "image/png")
    assert_not @user.save
  end

  test 'should not create user without phone_number' do
    @user = User.new(first_name: 'ahmed',
                     last_name: 'ibrahim',
                     country_code: 'EGY',
                     gender: 'male',
                     birthdate: '1994-02-13',
                     email: 'ah_ibrahim94@hotmail.com')
    @user.avatar.attach(io: File.open("public/test_image.png"),
                        filename: "test_image.png", content_type: "image/png")
    assert_not @user.save
  end

  test 'should not create user with invalid phone_number' do
    @user = User.new(first_name: 'ahmed',
                     last_name: 'ibrahim',
                     country_code: 'EGY',
                     gender: 'male',
                     phone_number: '6854156',
                     birthdate: '1994-02-13',
                     email: 'ah_ibrahim94@hotmail.com')
    @user.avatar.attach(io: File.open("public/test_image.png"),
                        filename: "test_image.png", content_type: "image/png")
    assert_not @user.save
  end

  test 'should not create user without gender' do
    @user = User.new(first_name: 'ahmed',
                     last_name: 'ibrahim',
                     country_code: 'EGY',
                     phone_number: '+201111831133',
                     birthdate: '1994-02-13',
                     email: 'ah_ibrahim94@hotmail.com')
    @user.avatar.attach(io: File.open("public/test_image.png"),
                        filename: "test_image.png", content_type: "image/png")
    assert_not @user.save
  end


  test 'should not create user with invalid gender' do
    @user = User.new(first_name: 'ahmed',
                     last_name: 'ibrahim',
                     country_code: 'EGY',
                     gender: 'm',
                     phone_number: '+201111831133',
                     birthdate: '1994-02-13',
                     email: 'ah_ibrahim94@hotmail.com')
    @user.avatar.attach(io: File.open("public/test_image.png"),
                        filename: "test_image.png", content_type: "image/png")
    assert_not @user.save
  end

  test 'should not create user without birthdate' do
    @user = User.new(first_name: 'ahmed',
                     last_name: 'ibrahim',
                     country_code: 'EGY',
                     phone_number: '+201111831133',
                     gender: 'male',
                     email: 'ah_ibrahim94@hotmail.com')
    @user.avatar.attach(io: File.open("public/test_image.png"),
                        filename: "test_image.png", content_type: "image/png")
    assert_not @user.save
  end
  test 'should not create user with invalid birthdate' do
    @user = User.new(first_name: 'ahmed',
                     last_name: 'ibrahim',
                     country_code: 'EGY',
                     phone_number: '+201111831133',
                     gender: 'male',
                     birthdate: '13/02/1994',
                     email: 'ah_ibrahim94@hotmail.com')
    @user.avatar.attach(io: File.open("public/test_image.png"),
                        filename: "test_image.png", content_type: "image/png")
    assert_not @user.save
  end
  test 'should not create user without avatar' do
    @user = User.new(first_name: 'ahmed',
                     last_name: 'ibrahim',
                     country_code: 'EGY',
                     phone_number: '+201111831133',
                     gender: 'male',
                     birthdate: '13/02/1994',
                     email: 'ah_ibrahim94@hotmail.com')
    assert_not @user.save
  end
  test 'should not create user with invalid avatar extension' do
    @user = User.new(first_name: 'ahmed',
                     last_name: 'ibrahim',
                     country_code: 'EGY',
                     phone_number: '+201111831133',
                     gender: 'male',
                     birthdate: '13/02/1994',
                     email: 'ah_ibrahim94@hotmail.com')
    @user.avatar.attach(io: File.open("public/test_pdf.pdf"),
                        filename: "test_pdf.pdf", content_type: "file/pdf")
    assert_not @user.save
  end
end
