# frozen_string_literal: true

class UsersController < ApplicationController
  skip_before_action :authenticate_request, only: :create

  def create
    @user = User.new(user_params)
    if @user.save
      render json: @user, status: :created
    else
      render json: ErrorSerializer.new(@user.errors), status: :unprocessable_entity
    end
  end

  def update_status
    command = PhoneNumberValidator.call(user_status_params[:phone_number])
    if command.success?
      render json: UserStatusSerializer.new(current_user, user_status_params[:status])
    else
      render json: { error: command.errors }, status: :bad_request
    end
  end

  private

  def user_params
    params.permit(:first_name, :last_name, :country_code,
                  :phone_number, :gender, :birthdate,
                  :email, :avatar, :password, :password_confirmation)
  end

  def user_status_params
    params.permit(:phone_number, :status)
  end
end
