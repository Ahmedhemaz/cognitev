# frozen_string_literal: true

class AuthenticateUser
  prepend SimpleCommand

  def initialize(phone_number, password)
    @phone_number = phone_number
    @password = password
  end

  def call
    JsonWebToken.encode(user_id: user.id) if user
  end

  private

  attr_accessor :phone_number, :password

  def user
    user = User.find_by_phone_number(phone_number)
    return user if user&.authenticate(password)

    errors.add :user_authentication, 'invalid credentials'
    nil
  end
end
