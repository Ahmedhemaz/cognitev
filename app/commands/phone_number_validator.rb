class PhoneNumberValidator
  prepend SimpleCommand
  def initialize(phone_number)
    @phone_number = phone_number
  end

  def call
    validate_phone_number
  end

  private

  def validate_phone_number
    pattern = /^\+?[0-9]\d{1,14}+\z$/
    if pattern.match?(@phone_number) && @phone_number.length > 10 && @phone_number.length < 15
      return true
    end
    errors.add :phone_number, :"invalid_phone_number"
  end

end