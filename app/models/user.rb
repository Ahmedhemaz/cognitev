# frozen_string_literal: true

class User < ApplicationRecord
  has_one_attached :avatar
  has_secure_password
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates_inclusion_of :country_code, in: %w[EGY FR GER], message: 'inclusion'
  validates_inclusion_of :gender, in: %w[male female], message: 'inclusion'
  validates :phone_number, length: { maximum: 15, minimum: 10 },
                           uniqueness: true,
                           format: { with: /\+?[0-9]\d{1,14}+\z/,
                                     message: 'not_a_number' },
                           presence: true
  validates :email, uniqueness: true,
                    format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i,
                              message: 'invalid email' }
  validates :birthdate, presence: true,
                        timeliness: { on_or_before: -> { Date.current },
                                      type: :date, format: 'yyyy-mm-dd' }
  validates :avatar, attached: true,
                     content_type: { in: %w[image/png image/jpg image/jpeg],
                                     message: 'Must be in (png,jpg,jpeg) format'}
end
