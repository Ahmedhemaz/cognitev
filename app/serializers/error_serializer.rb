# frozen_string_literal: true

class ErrorSerializer
  attr_accessor :errors
  def initialize(errors_object)
    @errors = errors_object.to_hash
    serialize_errors
  end

  def serialize_errors
    errors_hash = {}
    @errors.each do |key, value|
      errors_hash.store(key, convert_array_values_to_hashes(value))
    end
    @errors = errors_hash
    @errors.to_json
  end

  def convert_array_values_to_hashes(array)
    hashes_array = []
    array.each do |item|
      hashes_array.push({error:item})
    end
    hashes_array
  end
end
