class UserSerializer < ActiveModel::Serializer
  attribute :id, key: :id
  attribute :first_name, key: :first_name
  attribute :last_name, key: :last_name
  attribute :country_code, key: :country_code
  attribute :phone_number, key: :phone_number
  attribute :gender, key: :gender
  attribute :birthdate, key: :birthdate
end
