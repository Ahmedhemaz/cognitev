# frozen_string_literal: true

class UserStatusSerializer
  attr_accessor :user
  def initialize(user,status)
    @user = user
    @status = status
    serialize_user
  end

  def serialize_user
    @user = UserSerializer.new(@user).to_hash
  end
end
