class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :country_code
      t.string :phone_number
      t.string :gender
      t.string :birthdate
      t.string :email

      t.timestamps
    end
  end
end
