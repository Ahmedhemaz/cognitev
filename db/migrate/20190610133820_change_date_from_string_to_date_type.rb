class ChangeDateFromStringToDateType < ActiveRecord::Migration[5.2]
  def change
    change_column :users , :birthdate, :date
  end
end
