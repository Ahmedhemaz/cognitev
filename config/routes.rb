Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :users
  post 'authenticate', to: 'authentication#authenticate'
  post 'update_status', to: 'users#update_status'

end
